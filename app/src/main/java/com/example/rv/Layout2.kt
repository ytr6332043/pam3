package com.example.rv

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class Layout2 : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_layout2)
        val bundle: Bundle? = intent.extras
        val nim:TextView=findViewById(R.id.ni)
        val nama:TextView=findViewById(R.id.na)
        nim.text=bundle?.getString("nim")
        nama.text=bundle?.getString("nama")
    }
}