package com.example.rv

import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private lateinit var rv1: RecyclerView
    private lateinit var etNim: EditText
    private lateinit var etNama: EditText
    private lateinit var adapter: MahasiswaAdapter
    private val data: ArrayList<Mahasiswa> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rv1 = findViewById(R.id.rv1)
        etNim = findViewById(R.id.editTextTextPersonName2)
        etNama = findViewById(R.id.etNim)

        adapter = MahasiswaAdapter(this, data)
        rv1.adapter = adapter
        rv1.layoutManager = LinearLayoutManager(this)
    }

    fun tambahData(view: android.view.View) {
        val nim = etNim.text.toString()
        val nama = etNama.text.toString()
        val mahasiswa = Mahasiswa(nim, nama)
        data.add(mahasiswa)
        adapter.notifyDataSetChanged()
    }
}