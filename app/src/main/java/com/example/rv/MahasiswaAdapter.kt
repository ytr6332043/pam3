package com.example.rv

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MahasiswaAdapter(private val context: Context, private val data: ArrayList<Mahasiswa>) :
    RecyclerView.Adapter<MahasiswaAdapter.MahasiswaViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MahasiswaViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.row, parent, false)
        return MahasiswaViewHolder(view)
    }

    override fun onBindViewHolder(holder: MahasiswaViewHolder, position: Int) {
        val mahasiswa: Mahasiswa = data[position]
        val biodata = data[position]
        holder.itemView.setOnClickListener{
            val intent = Intent(context,Layout2::class.java)
            intent.putExtra("nim",biodata.nim)
            intent.putExtra("nama",biodata.nama)
            context.startActivity(intent)
        }
        holder.tvNama.text = mahasiswa.nama
        holder.tvNim.text = mahasiswa.nim
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class MahasiswaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvNim: TextView = itemView.findViewById(R.id.tvNim)
        val tvNama: TextView = itemView.findViewById(R.id.tvNama)
    }
}